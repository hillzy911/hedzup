//
//  ViewController.swift
//  hedzup
//
//  Created by Hilary Chipunza on 30/9/2016.
//  Copyright © 2016 Hilary Chipunza. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FBSDKCoreKit
import FBSDKLoginKit
import UCZProgressView


class WelcomeController: UIViewController
{
    
    private let facebookLogin = FBSDKLoginManager()
    var progressView: UCZProgressView?
    
    
   

    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.progressView = UCZProgressView(frame: self.view.bounds)
        self.progressView!.translatesAutoresizingMaskIntoConstraints = false
        self.progressView!.indeterminate = true
        self.progressView!.blurEffect = UIBlurEffect(style: .extraLight)
        self.progressView!.tintColor = UIColor(red:  41/255.0, green: 119/255.0, blue: 195/255.0, alpha: 10.0/100.0)
        
           //self.view!.addSubview(self.progressView!)
        // Do any additional setup after loading the view, typically from a nib.
    }

    
    override func viewDidAppear(_ animated: Bool)
    {
       
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    
    
    @IBAction func emailLogin(_ sender: AnyObject)
    {
        
    }
    
    @IBAction func toLoginScreen(_ sender: AnyObject)
    {
        
      
        
        
    }

   
    @IBAction func facebookLogin(_ sender: AnyObject)
    {
        let loginManager = FBSDKLoginManager()
        loginManager.logIn(withReadPermissions: ["email"], from: self, handler: { (result, error) in
            if error != nil
            {
                //self.showMessagePrompt(error.localizedDescription)
                print(error?.localizedDescription)
            }
            else if result!.isCancelled
            {
                print("FBLogin cancelled")
            } else
            
            {
                // [START headless_facebook_auth]
                let credential = FIRFacebookAuthProvider.credential(withAccessToken: FBSDKAccessToken.current().tokenString)
                
                Hedzup.sharedInstance.facebookToken = FBSDKAccessToken.current().tokenString
                self.view!.addSubview(self.progressView!)
                FIRAuth.auth()?.signIn(with: credential)
                { (user, error) in
                    if error != nil
                    {
                        print("Login failed. \(error)")
                        
                         self.progressView!.isHidden = true
                    } else
                    {
                        print(user?.displayName)
                         self.progressView!.isHidden = true
                        /*let viewController:UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Home")
                        self.present(viewController, animated: true, completion: nil)*/
                        
                        
                    }
                }

                
              
            }
        })
  

    }
    
    @IBAction func signUp(_ sender: AnyObject)
    {
        
    }
    
}

