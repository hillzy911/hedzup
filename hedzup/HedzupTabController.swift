//
//  HedzupTabController.swift
//  hedzup
//
//  Created by Hilary Chipunza on 1/10/2016.
//  Copyright © 2016 Hilary Chipunza. All rights reserved.
//

import UIKit
import LUNTabBarController

class HedzupTabController: LUNTabBarController
{
    
    var animationController: LUNTabBarAnimationController?
    var fadeAnimationController: DemoFadeAnimationController!
    var animation: UIViewControllerAnimatedTransitioning?
    var previousViewController: UIViewController!
    var dismissRecognizer: UIPanGestureRecognizer!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //DPTheme.customizeButton(UIColor.redColor())
        self.fadeAnimationController = DemoFadeAnimationController()
        self.animationController = LUNTabBarAnimationController()
        self.dismissRecognizer = UIPanGestureRecognizer(target: self, action: #selector(self.handleDismissGestureRecognizer))
        self.delegate = self
        
        
    }
    
    
    override func hideFloatingTab() {
        if self.selectedIndex == self.floatingTabIndex
        {
            self.selectedViewController = self.previousViewController
        }
    }
    
    
    
    func handleDismissGestureRecognizer()
    {
        
        switch self.dismissRecognizer.state
        {
        case .began:
            self.hideFloatingTab()
        case .changed:
            let translation: CGFloat = self.dismissRecognizer.translation(in: dismissRecognizer.view!.superview!).y
            var progress: CGFloat = translation / self.floatingContentHeight
            progress = max(0, min(1, progress))
            self.animationController?.update(progress)
            
        case .ended:
            let velocity: CGFloat = self.dismissRecognizer.velocity(in: self.dismissRecognizer.view!.superview!).y
            if velocity > 0
            {
                self.animationController!.finish()
            }
            else
            {
                self.animationController!.cancel()
            }
            
        case .cancelled:
            self.animationController!.cancel()
        default:
            break
        }
        
    }
    
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func tabBarController(_ tabBarController: UITabBarController, animationControllerForTransitionFrom fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning?
    {
    
            animation = super.tabBarController(tabBarController, animationControllerForTransitionFrom: fromVC, to: toVC)
            animation = self.fadeAnimationController
            self.fadeAnimationController.transitionDuration = self.transitionDuration
            return animation
        
    }
    
    
    override func tabBarController(_ tabBarController: UITabBarController, interactionControllerFor animationController: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning?
    {
        let isRecognizerActive = self.dismissRecognizer.state == .began || self.dismissRecognizer.state == .changed
        return isRecognizerActive ? self.animationController : nil
    }
    

    
}
