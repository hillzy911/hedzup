//
//  User.swift
//  hedzup
//
//  Created by Hilary Chipunza on 6/10/2016.
//  Copyright © 2016 Hilary Chipunza. All rights reserved.
//

import UIKit

class Users: NSObject {
    var name: String?
    var email: String?
    var profileImageUrl: String?
}
