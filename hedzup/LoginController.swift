//
//  LoginController.swift
//  hedzup
//
//  Created by Hilary Chipunza on 1/10/2016.
//  Copyright © 2016 Hilary Chipunza. All rights reserved.
//

import UIKit
import PasswordTextField

class LoginController: UIViewController {

    @IBOutlet weak var password: PasswordTextField!
    @IBOutlet weak var email: UITextField!
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        password.secureTextButton.buttonTouch()
        password.showButtonWhile = PasswordTextField.ShowButtonWhile(rawValue: "always")!
        let validationRule = RegexRule(regex:"^(?!=.*?[A-Z]).{6,}$", errorMessage: "Password Must Contain Atleast 6 Characters")
        password.layer.borderColor = UIColor(red:  235/255.0, green: 235/255.0, blue: 235/255.0, alpha: 100.0/100.0).cgColor
        password.layer.borderWidth = 1
        
        email.layer.borderColor = UIColor(red:  235/255.0, green: 235/255.0, blue: 235/255.0, alpha: 100.0/100.0).cgColor
        email.layer.borderWidth = 1

        password.validationRule = validationRule

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
 

    @IBAction func login(_ sender: AnyObject)
    {
        let viewController:UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "tabController")
         self.present(viewController, animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
